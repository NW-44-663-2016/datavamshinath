﻿using System;

namespace DataVamshinath.Models
{
    internal class ScallfoldColumnAttribute : Attribute
    {
        private bool v;

        public ScallfoldColumnAttribute(bool v)
        {
            this.v = v;
        }
    }
}