using System.Linq;
using Microsoft.AspNet.Mvc;
using Microsoft.AspNet.Mvc.Rendering;
using Microsoft.Data.Entity;
using DataVamshinath.Models;

namespace DataVamshinath.Controllers
{
    public class JobsController : Controller
    {
        private AppDbContext _context;

        public JobsController(AppDbContext context)
        {
            _context = context;    
        }

        // GET: Jobs
        public IActionResult Index()
        {
            return View(_context.Jobs.ToList());
        }

        // GET: Jobs/Details/5
        public IActionResult Details(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }

            Jobs jobs = _context.Jobs.Single(m => m.jobId == id);
            if (jobs == null)
            {
                return HttpNotFound();
            }

            return View(jobs);
        }

        // GET: Jobs/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Jobs/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(Jobs jobs)
        {
            if (ModelState.IsValid)
            {
                _context.Jobs.Add(jobs);
                _context.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(jobs);
        }

        // GET: Jobs/Edit/5
        public IActionResult Edit(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }

            Jobs jobs = _context.Jobs.Single(m => m.jobId == id);
            if (jobs == null)
            {
                return HttpNotFound();
            }
            return View(jobs);
        }

        // POST: Jobs/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(Jobs jobs)
        {
            if (ModelState.IsValid)
            {
                _context.Update(jobs);
                _context.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(jobs);
        }

        // GET: Jobs/Delete/5
        [ActionName("Delete")]
        public IActionResult Delete(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }

            Jobs jobs = _context.Jobs.Single(m => m.jobId == id);
            if (jobs == null)
            {
                return HttpNotFound();
            }

            return View(jobs);
        }

        // POST: Jobs/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public IActionResult DeleteConfirmed(int id)
        {
            Jobs jobs = _context.Jobs.Single(m => m.jobId == id);
            _context.Jobs.Remove(jobs);
            _context.SaveChanges();
            return RedirectToAction("Index");
        }
    }
}
