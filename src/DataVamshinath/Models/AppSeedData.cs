﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;

namespace DataVamshinath.Models
{

    public static class AppSeedData
    {

        public static void Initialize(IServiceProvider serviceProvider)
        {

            var context = serviceProvider.GetService<AppDbContext>();
            if (context.Database == null)
            {
                throw new Exception("DB is null");
            }
            if (context.Locations.Any())
            {
                return;   // DB already seeded
            }
            if (context.Jobs.Any())
            {
                return;   // DB already seeded
            }


            var loc1 = new Location() { Latitude = 40.3494179, Longitude = -94.9238399, Place = "Maryville", State = "Missouri", Country = "USA" };
            var loc2 = new Location() { Latitude = 37.6991993, Longitude = -97.4843859, Place = "Wichita", State = "Kansas", Country = "USA" };

            var loc3 = new Location() { Latitude = 40.3494179, Longitude = -94.9238399, Place = "Maryville", State = "Missouri", Country = "USA" };
            var loc4 = new Location() { Latitude = 45.0993041, Longitude = -93.1007215, Place = "Whte Bear Lake", State = "Minnesota", Country = "USA" };
            var loc5 = new Location() { Latitude = 17.4126272, Longitude = 78.2676166, Place = "Hyderabad", Country = "India" };
            var loc6 = new Location() { Latitude = 25.9019385, Longitude = 84.6797775, Place = "Bihar", Country = "India" };
            var loc7 = new Location() { Latitude = 29.9, Longitude = 100.5, Place = "Delhi", Country = "India" };
            context.AddRange(loc1, loc2, loc3, loc4, loc5, loc6, loc7);  
                context.Jobs.AddRange(
                    new Jobs() { jobId = 40, JobName = "Developer", eventLocation = loc1.LocationID },
             new Jobs() { jobId = 50, JobName = "Sr Consultant", eventLocation = loc2.LocationID },
              new Jobs() { jobId = 60, JobName = "Sr Android eveloper", eventLocation = loc3.LocationID },
               new Jobs() { jobId = 70, JobName = "Sr Ios Developer", eventLocation = loc4.LocationID },
                new Jobs() { jobId = 30, JobName = "Marketing Manager", eventLocation = loc5.LocationID },
                 new Jobs() { jobId = 20, JobName = "Desigining", eventLocation = loc6.LocationID },
                  new Jobs() { jobId = 10, JobName = "Manager", eventLocation = loc7.LocationID }
                  );


             context.SaveChanges();

            }

        }
    }

