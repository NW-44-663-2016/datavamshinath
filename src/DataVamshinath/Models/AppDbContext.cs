﻿using Microsoft.Data.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DataVamshinath.Models
{
    
        public class AppDbContext : DbContext
        {

            public DbSet<Location> Locations { get; set; }
            public DbSet<Jobs> Jobs { get; set; }

    }

    }

